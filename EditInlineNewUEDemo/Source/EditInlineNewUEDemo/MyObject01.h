// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MyObject01.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, editinlinenew, DefaultToInstanced)
class EDITINLINENEWUEDEMO_API UMyObject01 : public UObject
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString NameString;

};
